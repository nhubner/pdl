package pdl.backend;

import io.scif.FormatException;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.type.numeric.integer.UnsignedByteType;

import java.io.IOException;

public class Image {

	private static Long count = 0L;
	private final Long id;
	private final String size;
	private final String type;
	private byte[] data;
	private String name;

	public Image(final String name, String type, final byte[] data) throws FormatException {
		if (!isValidContentType(type)) {
			throw new FormatException(type);
		}
		id = count++;
		this.name = name;
		this.type = type;
		if (this.type.equals("image/tiff")) {
			System.out.println("tiff image");
			try {
				this.data = ImageConverter.imageToJPEGBytes(ImageConverter.imageFromJPEGBytes(data));
			} catch (IOException e) {
				e.printStackTrace();
				this.data = data;
				System.err.println("Error can't convert tiff bytes to jpeg bytes");
			}
		} else {
			this.data = data;
		}
		this.size = getDimensions();
	}

	public static boolean isValidContentType(String contentType) {
		contentType = contentType.toLowerCase();
		return contentType.equals("image/jpeg") || contentType.equals("image/tiff");
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	private String getDimensions() {
		SCIFIOImgPlus<UnsignedByteType> img;
		try {
			img = ImageConverter.imageFromJPEGBytes(data);
			int iw = (int) img.max(0) + 1;
			int ih = (int) img.max(1) + 1;
			int dimension = img.numDimensions();
			return iw + "*" + ih + "*" + dimension;
		} catch (FormatException e) {
			System.err.println("Bad format");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "?";
	}

	public String getSize() {
		return size;
	}

	public byte[] getData() {
		return data;
	}
}
