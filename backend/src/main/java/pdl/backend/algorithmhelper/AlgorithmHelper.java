package pdl.backend.algorithmhelper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.ubx.imageapi.BrightnessUtils;
import fr.ubx.imageapi.FilterUtils;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.*;

public class AlgorithmHelper {
	public static final float UNSPECIFIED_OPTION = -1048;

	static final String ALGORITHM_BRIGHTNESS = "brightness";
	static final String ALGORITHM_EQUALIZE = "equalize";
	static final String ALGORITHM_GAUSS = "gauss";
	static final String ALGORITHM_HUE = "hue";
	static final String ALGORITHM_MEAN = "mean";
	static final String ALGORITHM_OUTLINE = "outline";
	static ObjectMapper mapper = new ObjectMapper();

	final static Map<String, AlgorithmSpec> ALGORITHM_SPECS = Map.of(
			ALGORITHM_BRIGHTNESS, AlgorithmSpec.BRIGHTNESS,
			ALGORITHM_EQUALIZE, AlgorithmSpec.EQUALIZE,
			ALGORITHM_GAUSS, AlgorithmSpec.GAUSS,
			ALGORITHM_HUE, AlgorithmSpec.HUE,
			ALGORITHM_MEAN, AlgorithmSpec.MEAN,
			ALGORITHM_OUTLINE, AlgorithmSpec.OUTLINE
	);

	/**
	 * Check if parameters are valid. Return an empty optional is parameters are valid,
	 * else the optional contains the ResponseEntity corresponding to the error
	 *
	 * @param parameters the parameters to verify
	 * @return Empty optional if it is valid, an optional containing a ResponseEntity if not
	 */
	static public Optional<ResponseEntity<?>> areParametersValid(Map<String, String> parameters) {
		Map<String, String> copiedParameters = new HashMap<>(parameters);
		String algorithmName = copiedParameters.get("algorithm");
		copiedParameters.remove("algorithm");
		if (ALGORITHM_SPECS.containsKey(algorithmName)) {
			AlgorithmSpec algorithmSpec = getAlgorithmSpec(algorithmName);
			String optionName = algorithmSpec.getOptionName();
			if (copiedParameters.containsKey(optionName)) {
				float optionValue;
				try {
					optionValue = Float.parseFloat(copiedParameters.get(optionName));
				} catch (NumberFormatException e) {
					return Optional.of(ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body("Incorrect value for " + optionName + "."));
				}
				if (!checkOptionValue(algorithmSpec, optionValue)) {
					return Optional.of(ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body(optionName + " should be between " + algorithmSpec.getMin() + " and " + algorithmSpec.getMax() + " (actual: " + optionValue + ")."));
				}
				copiedParameters.remove(optionName);
				if (copiedParameters.isEmpty()) {
					return Optional.empty();
				} else {
					return Optional.of(ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body("Wrong parameters: " + copiedParameters));
				}
			} else if (algorithmName.equals(ALGORITHM_OUTLINE) || algorithmName.equals(ALGORITHM_EQUALIZE)) {
				return Optional.empty();
			} else {
				return Optional.of(ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body("Missing parameter : " + optionName));
			}
		} else {
			return Optional.of(ResponseEntity.status(400).contentType(MediaType.TEXT_PLAIN).body("Algorithm " + algorithmName + " not supported"));
		}
	}

	static public Optional<ResponseEntity<?>> areParametersValid(JsonNode jsonNode) {
		ObjectMapper mapper = new ObjectMapper();
		return areParametersValid(
				mapper.convertValue(jsonNode,
						new TypeReference<Map<String, String>>() {
						})
		);
	}

	static public AlgorithmSpec getAlgorithmSpec(String algorithmName) {
		return ALGORITHM_SPECS.get(algorithmName);
	}

	static public String getOptionName(String algorithmName) {
		return getAlgorithmSpec(algorithmName).getOptionName();
	}

	static private boolean checkOptionValue(AlgorithmSpec algorithmSpec, float optionValue) {
		return optionValue >= algorithmSpec.getMin() && optionValue <= algorithmSpec.getMax();
	}

	public static Img<UnsignedByteType> apply(Img<UnsignedByteType> img, String algorithmName, float optionValue) throws UnsupportedAlgorithmException {
		if (algorithmName.equals(ALGORITHM_HUE))
			return FilterUtils.hueFilter(img, optionValue);
		else if (algorithmName.equals(ALGORITHM_GAUSS))
			return FilterUtils.gaussBlurFilter(img, optionValue);
		else
			return apply(img, algorithmName, (int) optionValue);
	}

	private static Img<UnsignedByteType> apply(Img<UnsignedByteType> img, String algorithmName, int optionValue) throws UnsupportedAlgorithmException {
		switch (algorithmName) {
			case ALGORITHM_BRIGHTNESS:
				return BrightnessUtils.changeBrightness(img, optionValue);
			case ALGORITHM_EQUALIZE:
				if (optionValue == 1 || optionValue == 2) {
					return BrightnessUtils.equalizeHistogramColor(img, optionValue);
				} else {
					return BrightnessUtils.equalizeHistogramGreyScale(img);
				}
			case ALGORITHM_MEAN:
				return FilterUtils.meanBlurFilter(img, optionValue);
			case ALGORITHM_OUTLINE:
				if (optionValue == UNSPECIFIED_OPTION) {
					return FilterUtils.outlineFilter(img);
				} else {
					return FilterUtils.outlineFilter(img, optionValue);
				}
			default:
				throw new UnsupportedAlgorithmException(algorithmName + ": unsupported algorithm");
		}
	}

	static public JsonNode getAlgorithmSpecsAsJSON(String algorithmName) {
		AlgorithmSpec algorithmSpec = getAlgorithmSpec(algorithmName);
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("name", algorithmName)
				.set("parameter", algorithmSpec.getOptionSpecsAsJSON());
		return rootNode;
	}

	static public ArrayNode getAlgorithmsListAsJSON() {
		ArrayNode arrayNode = mapper.createArrayNode();
		SortedSet<String> keys = new TreeSet<>(ALGORITHM_SPECS.keySet());
		for (String key : keys) {
			arrayNode.add(getAlgorithmSpecsAsJSON(key));
		}
		return arrayNode;
	}

}
