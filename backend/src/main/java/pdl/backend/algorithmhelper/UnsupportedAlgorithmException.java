package pdl.backend.algorithmhelper;

public class UnsupportedAlgorithmException extends Exception {
	public UnsupportedAlgorithmException(String message) {
		super(message);
	}
}
