package pdl.backend.algorithmhelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public enum AlgorithmSpec {
	BRIGHTNESS("delta", -500, 500, true, true),
	EQUALIZE("channel", 1, 2, false, true),
	GAUSS("sigma", 0, 50, true),
	HUE("value", 0, 359, true),
	MEAN("level", 0, 50, true, true),
	OUTLINE("threshold", 0, 255, false, true);

	private final String optionName;
	private final float min;
	private final float max;
	private final boolean requiredOption;
	private final boolean restrictToInt;
	private final ObjectMapper mapper = new ObjectMapper();

	AlgorithmSpec(String optionName, float min, float max, boolean requiredOption) {
		this(optionName, min, max, requiredOption, false);
	}

	AlgorithmSpec(String optionName, float min, float max, boolean requiredOption, boolean restrictToInt) {
		this.optionName = optionName;
		this.min = min;
		this.max = max;
		this.requiredOption = requiredOption;
		this.restrictToInt = restrictToInt;
	}

	public String getOptionName() {
		return optionName;
	}

	public float getMin() {
		return min;
	}

	public float getMax() {
		return max;
	}

	public boolean isRequiredOption() {
		return requiredOption;
	}

	@Override
	public String toString() {
		return "AlgorithmSpec{" +
				"option='" + optionName + '\'' +
				", min=" + min +
				", max=" + max +
				", requiredOption=" + requiredOption +
				", restrictToInt=" + restrictToInt +
				'}';
	}

	public ObjectNode getOptionSpecsAsJSON() {
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("name", optionName);
		if (restrictToInt) {
			rootNode.put("type", "int")
					.put("min", (int) min)
					.put("max", (int) max);
		} else {
			rootNode.put("type", "float")
					.put("min", min)
					.put("min", max);
		}
		rootNode.put("optional", !requiredOption);
		return rootNode;
	}
}
