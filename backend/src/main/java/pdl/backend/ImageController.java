package pdl.backend;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.scif.FormatException;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pdl.backend.algorithmhelper.AlgorithmHelper;
import pdl.backend.algorithmhelper.UnsupportedAlgorithmException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

import static pdl.backend.ImageConverter.imageFromJPEGBytes;

@RestController
@RequestMapping("/")
@Validated
public class ImageController {
	private final ImageDao imageDao;
	@Autowired
	private ObjectMapper mapper;

	@Autowired
	public ImageController(ImageDao imageDao) {
		this.imageDao = imageDao;
	}

	private ResponseEntity<?> createResponseEntity(SCIFIOImgPlus<UnsignedByteType> originalImgPlus, Img<UnsignedByteType> img) {
		try {
			SCIFIOImgPlus<UnsignedByteType> resultImage = new SCIFIOImgPlus<>(img);
			resultImage.setMetadata(originalImgPlus.getMetadata());
			byte[] data = ImageConverter.imageToJPEGBytes(resultImage);
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(data);
		} catch (IOException | FormatException e) {
			e.printStackTrace();
			return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
		}
	}

	/**
	 * Return the image corresponding to the id if present, or null if not present
	 *
	 * @param id the id of the image to retrieve
	 * @return the image if present, null if not present
	 */
	private SCIFIOImgPlus<UnsignedByteType> retrieveImage(long id) {
		Optional<Image> optional = imageDao.retrieve(id);
		if (optional.isEmpty()) {
			return null;
		} else {
			try {
				return imageFromJPEGBytes(optional.get().getData());
			} catch (FormatException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	@RequestMapping(value = "/images/{id}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.TEXT_PLAIN_VALUE, "image/tiff"})
	public ResponseEntity<?> getImage(@PathVariable("id") long id, @RequestParam Map<String, String> requestParams) {
		if (!requestParams.isEmpty()) {
			return ResponseEntity.badRequest().contentType(MediaType.TEXT_PLAIN).body("Wrong parameters: " + requestParams);
		}
		Optional<Image> optional = imageDao.retrieve(id);
		if (optional.isPresent()) {
			Image img = optional.get();
			MediaType mediaType = MediaType.parseMediaType(img.getType());
			byte[] data = img.getData();
			return ResponseEntity.ok().contentType(mediaType).body(data);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(value = "/images/{id}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.TEXT_PLAIN_VALUE, "image/tiff"}, params = {"algorithm"})
	public ResponseEntity<?> getImageWithAlgorithm(@PathVariable long id, @RequestParam Map<String, String> requestParams) throws UnsupportedAlgorithmException {
		System.out.println("deux");
		Optional<ResponseEntity<?>> response = AlgorithmHelper.areParametersValid(requestParams);
		if (response.isEmpty()) {
			String algorithmName = requestParams.get("algorithm");
			String optionName = AlgorithmHelper.getOptionName(algorithmName);
			float optionValue = AlgorithmHelper.UNSPECIFIED_OPTION;
			try {
				optionValue = Float.parseFloat(requestParams.get(optionName));
			} catch (Exception e) {
				e.printStackTrace();
			}
			SCIFIOImgPlus<UnsignedByteType> originalImgPlus = this.retrieveImage(id);
			if (originalImgPlus != null) {
				Img<UnsignedByteType> img = originalImgPlus.getImg();
				System.out.println("Applying " + algorithmName + " with " + optionName + " of " + optionValue + " on image " + id);
				try {
					img = AlgorithmHelper.apply(img, algorithmName, optionValue);
				} catch (Exception e) {
					return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body("Unable to apply algorithm: " + e.getMessage());
				}
				return this.createResponseEntity(originalImgPlus, img);
			} else {
				return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body("Can not find the image");
			}
		} else {
			return response.get();
		}
	}

	@RequestMapping(value = "/images/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> postImage(@PathVariable("id") long id, @RequestBody ArrayNode requestedAlgorithms) {
		System.out.println(requestedAlgorithms);
		SCIFIOImgPlus<UnsignedByteType> originalImgPlus = this.retrieveImage(id);
		if (originalImgPlus != null) {
			Img<UnsignedByteType> img = originalImgPlus.getImg();
			for (JsonNode innerNode : requestedAlgorithms) {
				Optional<ResponseEntity<?>> response = AlgorithmHelper.areParametersValid(innerNode);
				if (response.isEmpty()) {
					String algorithmName = innerNode.get("algorithm").asText();
					String optionName = AlgorithmHelper.getOptionName(algorithmName);
					float optionValue = AlgorithmHelper.UNSPECIFIED_OPTION;
					try {
						optionValue = Float.parseFloat(innerNode.get(optionName).asText());
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("+ Applying " + algorithmName + " with " + optionName + " of " + optionValue + " on image " + id);
					try {
						img = AlgorithmHelper.apply(img, algorithmName, optionValue);
					} catch (Exception e) {
						return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body("Unable to apply algorithm: " + e.getMessage());
					}
				} else {
					return response.get();
				}
			}
			return this.createResponseEntity(originalImgPlus, img);
		} else {
			return ResponseEntity.status(500).contentType(MediaType.TEXT_PLAIN).body("Can not find the image");
		}
	}

	@RequestMapping(value = "/images/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteImage(@PathVariable("id") long id) {
		Optional<Image> imageOptional = imageDao.retrieve(id);
		if (imageOptional.isPresent()) {
			imageDao.delete(imageOptional.get());
			return ResponseEntity.ok("image " + id + "deleted");
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/images", method = RequestMethod.POST)
	public ResponseEntity<?> addImage(@RequestParam("file") MultipartFile file) {
		try {
			String contentType = file.getContentType();
			if (Image.isValidContentType(contentType)) {
				Image img = new Image(file.getOriginalFilename(), contentType, file.getBytes());
				imageDao.create(img);
				URI location = new URI("/images/" + img.getId());
				return ResponseEntity.created(location).build();
			} else {
				return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
			}
		} catch (IOException | FormatException | URISyntaxException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/images", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ArrayNode getImageList() {
		ArrayNode nodes = mapper.createArrayNode();
		imageDao.retrieveAll().forEach(image -> nodes.add(mapper.createObjectNode()
				.put("id", image.getId())
				.put("name", image.getName())
				.put("type", image.getType())
				.put("size", image.getSize())));
		return nodes;
	}

	@RequestMapping(value = "/images/algorithms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ArrayNode getAlgorithmList() {
		return AlgorithmHelper.getAlgorithmsListAsJSON();
	}
}
