package pdl.backend;

import io.scif.FormatException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.MediaTypeFactory;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Repository
public class ImageDao implements Dao<Image> {

	private final Map<Long, Image> images = new HashMap<>();

	public ImageDao() {
		ClassPathResource imagesDirRes = new ClassPathResource("images");
		if (!imagesDirRes.exists()) {
			System.err.println("Image directory does not exist - skipping scan");
		} else {
			try {
				scanDir(imagesDirRes.getFile().toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	void scanDir(Path imagesDir) {
		if (!Files.exists(imagesDir) || !Files.isDirectory(imagesDir)) {
			System.err.println("directory \"" + imagesDir + "\" does not exist - skipping scan");
		} else {
			System.out.println("Scanning directory \"" + imagesDir + "\"...");
			try {
				Files.walk(imagesDir)
						.filter(Files::isReadable)    // read permission
						.filter(Files::isRegularFile) // file only
						.forEach(path -> loadImage(path.normalize()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private Boolean loadImage(ClassPathResource path) {
		try {
			return loadImage(path.getFile().toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Boolean loadImage(String path) {
		return loadImage(Paths.get(path));
	}

	private Boolean loadImage(Path path) {
		System.out.println("Loading \"" + path.toString() + "\"... ");
		try {
			String name = path.getFileName().toString();

			Optional<MediaType> optionalMediaType = MediaTypeFactory.getMediaType(name);
			if (optionalMediaType.isEmpty()) {
				System.err.println("Can't parse mime type");
				return false;
			}
			MediaType mediaType = optionalMediaType.get();
			String contentType = mediaType.toString();

			byte[] fileContent = Files.readAllBytes(Paths.get(path.toString()));

			Image img = new Image(name, contentType, fileContent);
			images.put(img.getId(), img);
			return true;
		} catch (NoSuchFileException e) {
			System.err.println(": KO -> No such file");
		} catch (FormatException e) {
			System.err.println(": KO -> Invalid format (" + e.getMessage() + ")");
		} catch (IOException e) {
			System.err.println(": KO");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Optional<Image> retrieve(final long id) {
		Image value = images.get(id);
		return value == null ? Optional.empty() : Optional.of(value);
	}

	@Override
	public List<Image> retrieveAll() {
		return new ArrayList<>(images.values());
	}

	@Override
	public void create(final Image img) {
		images.put(img.getId(), img);
	}

	@Override
	public void update(final Image img, final String[] params) {
		// Not used
	}

	@Override
	public void delete(final Image img) {
		images.remove(img.getId());
	}
}
