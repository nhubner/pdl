package pdl.backend;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.file.Files;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class ImageControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@BeforeAll
	public static void reset() {
		// reset Image class static counter
		ReflectionTestUtils.setField(Image.class, "count", 0L);
	}

	@Test
	@Order(1)
	public void getImageListShouldReturnSuccess() throws Exception {
		mockMvc.perform(get("/images"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print());
	}

	@Test
	@Order(2)
	public void getTIFImageShouldReturnSuccess() throws Exception {
		mockMvc.perform(get("/images/0"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("image/tiff"));
	}

	@Test
	@Order(3)
	public void getJPGImageShouldReturnSuccess() throws Exception {
		mockMvc.perform(get("/images/1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE));
	}

	@Test
	@Order(4)
	public void getImageShouldReturnNotFound() throws Exception {
		mockMvc.perform(get("/images/4")).andExpect(status().isNotFound());
	}

	@Test
	@Order(5)
	public void deleteExistingImage() throws Exception {
		mockMvc.perform(delete("/images/0")).andExpect(status().isOk())
				.andDo(mvcResult -> mockMvc.perform(get("/images/0")).andExpect(status().isNotFound()));
	}

	@Test
	@Order(6)
	public void deleteNonExistingImageShouldReturnNotFound() throws Exception {
		mockMvc.perform(delete("/images/42")).andExpect(status().isNotFound());
	}

	@Test
	@Order(7)
	public void createImageShouldReturnCreated() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file", "street.jpeg", "image/jpeg", Files.readAllBytes(new ClassPathResource("images/street.jpeg").getFile().toPath()));
		MvcResult req = mockMvc.perform(MockMvcRequestBuilders.multipart("/images").file(file))
				.andExpect(status().isCreated())
				.andReturn();
		mockMvc.perform(get(req.getResponse().getRedirectedUrl())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE));
	}

	@Test
	@Order(8)
	public void createImageShouldReturnUnsupportedMediaType() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file", "github.png", "image/png", Files.readAllBytes(new ClassPathResource("images/github.png").getFile().toPath()));
		mockMvc.perform(MockMvcRequestBuilders.multipart("/images").file(file))
				.andExpect(status().isUnsupportedMediaType());
	}

	@Test
	@Order(9)
	public void applyBrightnessShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_brightness_50.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "brightness")
				.param("delta", "50"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(10)
	public void applyEqualizeShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/street_equalize.jpg").getFile().toPath());
		mockMvc.perform(get("/images/3")
				.param("algorithm", "equalize"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(11)
	public void applyEqualizeSaturationShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_equalize_channel_1.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "equalize")
				.param("channel", "1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(12)
	public void applyEqualizeBrightnessShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_equalize_channel_2.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "equalize")
				.param("channel", "2"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(13)
	public void applyHueShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_hue_30.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "hue")
				.param("value", "30"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(14)
	public void applyMeanShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_mean_20.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "mean")
				.param("level", "20"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(15)
	public void applyGaussShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/flower_gauss_sigma_5.5.jpg").getFile().toPath());
		mockMvc.perform(get("/images/1")
				.param("algorithm", "gauss")
				.param("sigma", "5.5"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(16)
	public void applyOutlineSobelShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/street_outline.jpg").getFile().toPath());
		mockMvc.perform(get("/images/3")
				.param("algorithm", "outline"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(17)
	public void applyOutlineThresholdShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/street_outline_threshold_100.jpg").getFile().toPath());
		mockMvc.perform(get("/images/3")
				.param("algorithm", "outline")
				.param("threshold", "100"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(18)
	public void applyOutlineShouldReturnBadRequest() throws Exception {
		mockMvc.perform(get("/images/2")
				.param("algorithm", "outline")
				.param("threshold", "256"))
				.andExpect(status().isBadRequest())
				.andDo(mvcResult -> mockMvc.perform(get("/images/2")
						.param("algorithm", "outline")
						.param("threshold", "foo"))
						.andExpect(status().isBadRequest()));
	}

	@Test
	@Order(19)
	public void applyEqualizeShouldReturnBadRequest() throws Exception {
		mockMvc.perform(get("/images/2")
				.param("algorithm", "equalize")
				.param("channel", "3"))
				.andExpect(status().isBadRequest())
				.andDo(mvcResult -> mockMvc.perform(get("/images/2")
						.param("algorithm", "equalize")
						.param("channel", "-3"))
						.andExpect(status().isBadRequest()))
				.andDo(mvcResult -> mockMvc.perform(get("/images/2")
						.param("algorithm", "equalize")
						.param("channel", "bar"))
						.andExpect(status().isBadRequest()));
	}

	@Test
	@Order(20)
	public void applyHueShouldReturnBadRequest() throws Exception {
		mockMvc.perform(get("/images/2")
				.param("algorithm", "hue")
				.param("value", "360"))
				.andExpect(status().isBadRequest())
				.andDo(mvcResult -> mockMvc.perform(get("/images/2")
						.param("algorithm", "hue")
						.param("value", "foobar"))
						.andExpect(status().isBadRequest()));
	}

	@Test
	@Order(21)
	public void applyBadAlgorithmShouldReturnBadRequest() throws Exception {
		mockMvc.perform(get("/images/2")
				.param("algorithm", "fake"))
				.andExpect(status().isBadRequest());
	}

	@Test
	@Order(22)
	public void applyEqualizeAlgorithmShouldReturnInternalServer() throws Exception {
		mockMvc.perform(get("/images/1")
				.param("algorithm", "equalize"))
				.andExpect(status().isInternalServerError());
	}

	@Test
	@Order(23)
	public void badParameterShouldReturnBadRequest() throws Exception {
		mockMvc.perform(get("/images/1")
				.param("foo", "bar"))
				.andExpect(status().isBadRequest())
				.andDo(mvcResult -> mockMvc.perform(
						get("/images/2")
								.param("algorithm", "brightness")
								.param("delta", "10")
								.param("foo", "bar"))
						.andExpect(status().isBadRequest()));
	}

	@Test
	@Order(24)
	public void applySeveralAlgorithms1ShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/several_algorithms_1.jpg").getFile().toPath());
		String body = "[{\"algorithm\":\"brightness\",\"delta\":20},{\"algorithm\":\"hue\",\"value\":150},{\"algorithm\":\"mean\",\"level\":3}]";
		mockMvc.perform(post("/images/1").contentType(MediaType.APPLICATION_JSON)
				.content(body))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(25)
	public void applySeveralAlgorithms2ShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/several_algorithms_2.jpg").getFile().toPath());
		String body = "[{\"algorithm\":\"outline\",\"threshold\":14},{\"algorithm\":\"gauss\",\"sigma\":20},{\"algorithm\":\"equalize\"}]";
		mockMvc.perform(post("/images/3").contentType(MediaType.APPLICATION_JSON)
				.content(body))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(26)
	public void applyBrightnessUsingPostShouldReturnSuccess() throws Exception {
		byte[] expectedContent = Files.readAllBytes(new ClassPathResource("imgs/stock-1_brightness_-500.jpg").getFile().toPath());
		String body = "[{\"algorithm\":\"brightness\",\"delta\":-500}]";
		mockMvc.perform(post("/images/2").contentType(MediaType.APPLICATION_JSON)
				.content(body))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
				.andExpect(content().bytes(expectedContent));
	}

	@Test
	@Order(27)
	public void applySeveralAlgorithmsShouldReturnBadRequest() throws Exception {
		String body = "[{\"algorithm\":\"toto\",\"delta\":-500}]";
		mockMvc.perform(post("/images/2").contentType(MediaType.APPLICATION_JSON)
				.content(body))
				.andExpect(status().isBadRequest());
	}

	@Test
	@Order(28)
	public void getAlgorithmListShouldReturnSuccess() throws Exception {
		mockMvc.perform(get("/images/algorithms"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print());
	}

}
