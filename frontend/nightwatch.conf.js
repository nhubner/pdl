module.exports = {
	"src_folders" : ["tests_nightwatch"],

	"webdriver" : {
		"start_process": true,
		"server_path": require('chromedriver').path,
		"port": 5555
	},

	"test_settings" : {
		"default" : {
			"launch_url" : "http://localhost:8089",
			"desiredCapabilities": {
				"browserName": "chrome"
			}
		}
	}
}