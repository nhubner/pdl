module.exports = {
	'Add an image' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.waitForElementVisible('body')
		.assert.visible('input[type=file]')	
		.setValue('input[type="file"]', require('path').resolve('../backend/src/test/resources/imgs/flower_brightness_50.jpg'))
		.click('#submit button')
		.pause(1000)
		.useXpath().assert.attributeContains('//div[@id="gallery"]/img[5]', 'src', 'http://localhost:8089/images/4')
		.useXpath().assert.attributeContains('//div[@id="gallery"]/img[5]', 'alt', 'flower_brightness_50.jpg')
		.end();
	},

	'Delete an image' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.useCss().waitForElementVisible('body')
		.assert.elementPresent('#gallery img')
        .useXpath().click('//div[@id="gallery"]/img[5]')  
		.useCss().assert.containsText('#name-icon', 'flower_brightness_50...')
        .useXpath().click('//div[@id="click-icon-right"]/div[1]/img')
        .pause(1000)
        .useXpath().assert.not.elementPresent('//div[@id="gallery"]/img[5]')
		.end();
	}
}