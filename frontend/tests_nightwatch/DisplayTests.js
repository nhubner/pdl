module.exports = {
	'Select an image' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.waitForElementVisible('body')
		.assert.elementPresent('#gallery img')
        .click('#gallery img')
		.assert.containsText('#name-icon', 'face.tif')
        .useXpath().click('//div[@id="gallery"]/img[3]')  
		.useCss().assert.containsText('#name-icon', 'stock-1.jpg')
        .useXpath().click('//div[@id="click-icon-left"]/div[1]/img')
		.useCss().assert.containsText('#no-image-container', 'No image selected')
		.end();
	},

    'Display metadata' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.waitForElementVisible('body')
		.assert.elementPresent('#gallery img')
        .click('#gallery img')
		.assert.containsText('#name-icon', 'face.tif')
        .useXpath().click('//div[@id="click-icon-right"]/div[3]/img')   
        .pause(1000)
        .useCss().assert.elementPresent('#metadata-preview')
        .assert.containsText('#metadata-preview', '{}')   
        .useXpath().click('//div[@id="click-icon-right"]/div[3]/img')  
        .useCss().assert.not.elementPresent('#metadata-preview')
		.end();
	},

    'Display edit mode' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.waitForElementVisible('body')
		.assert.elementPresent('#gallery img')
        .useXpath().click('//div[@id="gallery"]/img[2]')  
		.useCss().assert.containsText('#name-icon', 'flower.jpg')
        .useXpath().click('//div[@id="click-icon-right"]/div[4]/img')
        .pause(1000)
        .useCss().assert.elementPresent('.algo-icon')
        .assert.elementPresent('#algo-title')
        .assert.containsText('#algo-title', 'brightness')
        .useXpath().click('//div[@id="click-icon-right"]/div[4]/img')
        .useCss().assert.not.elementPresent('.algo-icon')
		.end();
	}
}