module.exports = {
	'Get basic elements' : function(browser) {
		browser
        .maximizeWindow()
		.url('http://localhost:8089/')
		.waitForElementVisible('body')
		.assert.urlContains('local')
		.assert.title('Image editor')
		.assert.visible('input[type=file]')		
		.assert.elementPresent("img")
		.assert.containsText('#no-image-container', 'No image selected')
		.end();
	}
}