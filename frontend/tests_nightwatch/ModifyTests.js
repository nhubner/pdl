module.exports = {
    'Modify image' : function(browser) {
        browser
        .maximizeWindow()
        .url('http://localhost:8089/')
        .waitForElementVisible('body')
        .assert.elementPresent('#gallery img')
        .useXpath().click('//div[@id="gallery"]/img[2]')  
        .useCss().assert.containsText('#name-icon', 'flower.jpg')
        .assert.attributeContains('#image-preview', 'src', 'http://localhost:8089/images/1')
        .useXpath().click('//div[@id="click-icon-right"]/div[4]/img')
        .pause(1000)
        .useXpath().assert.elementPresent('//div[@class="algo-icon"]/div[6]')
        .assert.elementPresent('//div[@class="algo-icon"]/div[6]/span')
        .assert.containsText('//div[@class="algo-icon"]/div[6]/span', 'outline')
        .assert.elementPresent('//label[@id="algo-checkbox"]/input')
        .click('//label[@id="algo-checkbox"]/input')
        .click('//div[@class="algo-icon"]/button')
        .pause(4000)
        .useCss().assert.not.attributeContains('#image-preview', 'src', 'http://localhost:8089/images/1')
        .end();
    }
}