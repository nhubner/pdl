# Projet client-serveur

## Installation

Ce projet utilise Maven pour les modules backend et api, et NodeJS pour le module frontend.

Nous avons effectivement choisi de séparer le code qui fait le traitement d'images et le code du serveur.
De cette manière, le traitement d'images est sous la forme d'une bibliothèque qu'il suffit d'importer dans le backend.
Pour cela, il faut déployer la bibliothèque sur un dépôt maven. Celui-ci est en local dans le dossier /api/fakemvnrepo.  

Il est donc nécessaire d'exécuter la commande `mvn deploy` pour envoyer la bibliothèque sur le dépôt local.
Une fois cela fait, il est possible d'exécuter le module backend.

Les commandes permettant leur exécution (chacune dans le dossier correspondant) :  
Module api : `mvn package`  
Module backend : `mvn spring-boot:run`  
Module frontend : `npm run serve`

## Documentation

Pour le module api (algorithmes de modification d'images), une documentation sous forme de javadoc est présente pour toutes les méthodes.
De plus, un système de log a été mis en place pour savoir si les algorithmes fonctionnent correctement à tout moment.

Pour le module frontend, des commentaires ont été ajoutés au code afin de rendre celui-ci plus compréhensible.

## Tests

### Intégration continue

Des tests ont été produits pour la partie algorithme (module api) pour vérifier que ces algorithmes fonctionnais correctement.
Malheureusement, les algorithmes sont sur des images ce qui rend compliqué la vérification pixel par pixel.
On a pu seulement s'assurer que les méthodes ne stoppent pas l'exécution du programme, qu'elles ne lèvent pas d'exceptions, sauf quand c'est nécessaire.

Il en est de même pour le module backend. Toutefois, pour s'exécuter convenablement, il est nécessaire de les lancer sans modification du dossier '/backend/src/main/resources/images`.
En effet, des tests sur les algorithmes vérifient la concordance du corps de la réponse. La modification de ce dossier pouvant changer les identifiants des images, et in fine le corps.

Pour les deux modules, le lancement des tests, s'effectue via `mvn test`.

### Compatibilité du serveur

|OS|Testé|
|:---:|:---:|
|Windows 10|OUI|
|Ubuntu 20|NON|
|Debian Buster|OUI|
|MacOs 11|NON|

### Compatibilité du client

|Navigateur|Testé|
|:---:|:---:|
|Safari|NON|
|Chrome|OUI|
|Firefox|OUI|

