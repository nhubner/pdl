L'URL pour utiliser des algorithmes est de la forme suivante (évidemment, les valeurs entre chevrons sont à remplacer) :  
`/images/<id>?algorithm=<algorithm_name>`  
Selon l'algorithme choisi, il peut y avoir d'autres arguments à rajouter.  
Exemple : `/images/2?algorithm=brightness&delta=10`

|Algorithme|Arguments|description|
|:---:|:---:|:---|
|`brightness`|`delta` (int)|Incrémente ou décrémente la luminosité de l'image. `delta` correspond à la valeur d'incrémentation de la luminosité.|
|`equalize`|` `|Égalise l'histogramme de l'image. Celle-ci est en dégradé de gris|
|`equalize`|`channel` (int)|Égalise l'histogramme de l'image. Celle-ci est en couleur. `channel` est le canal sur lequel l'égalisation se fait. 1 pour utiliser la saturation, 2 pour utiliser la luminosité|
|`hue`|`value` (float)|Change la teinte de l'image par la valeur passée en argument. Cette teinte doit être dans l'intervalle [0-360[.|
|`mean`|`level` (int)|Applique un filtre moyenneur sur l'image (effet de flou / lissage). Le niveau du filtre est celui passé en argument. Plus le niveau est fort, plus le flou est fort.|
|`gauss`|`sigma` (float)|Applique un filtre gaussien sur l'image (effet de flou / lissage). `sigma` correspond à la déviation de la courbe gaussienne, c'est-à-dire plus `sigma` est grand, plus le flou est fort.|
|`outline`|` `|Applique une détection de contour sur l'image (via le filtre de Sobel).|
|`outline`|`threshold` (int)|Applique une détection de contour sur l'image, puis un seuillage. La valeur de ce seuillage, `threshold`, doit être dans l'intervalle [0-256[. Le seuillage sert à déterminer la quantité de contours que l'on veut garder. Un seuillage élevé indique que l'on ne veut que des contours forts.|
