# Fonctionnalités ajoutées

## Meilleure séparation des classes Java

Nous avons mieux séparé la gestion des algorithmes et la gestion des routes du serveur.
Les routes sont restées dans les mêmes classes, mais les méthodes qui servaient à faire appliquer les algorithmes
ont été déplacées dans de nouvelles classes dans le package `algorithmehelper`.  
De cette manière, nous avons un code plus propre et plus facile à lire.

## Enchaîner les algorithmes

Il est possible d'envoyer au serveur une liste d'algorithmes à utiliser sur une image.
Ceux-ci sont successivement appliqués sur l'image d'origine et l'image modifiée est envoyée au client.

## Liste des algorithmes

Une nouvelle route qui permet d'obtenir la liste des algorithmes ainsi que leurs spécificités a été ajouté. L'URL est `/images/algorithms`.  
Cette liste permet au client d'être "data-driven", c'est-à-dire qu'il produit un résultat en fonction des données contenues dans cette liste.
De cette manière, si un nouvel algorithme est ajouté à la liste, le client sera automatiquement mis à jour, sans changer le code JavaScript.

## Amélioration des fonctionnalités déjà présentes

- Meilleur algorithme de contour : Dans le premier rendu, appliquer un filtre de contour sur une image en couleur produisait un mauvais résultat.
  Désormais, le résultat est correct. D'abord l'image est convertie en nuance de gris, ensuite un filtre de contour est appliqué sur cette image.
- Meilleure interface utilisateur : Le client a été amélioré pour avoir une interface plus agréable et plus intuitive.
- Afficher les images TIFF : Le serveur convertit à la volée les images TIFF en image JPG quand il les envoie au client pour qu'il puisse correctement les afficher.
- Alerte pour l'utilisateur : Dorénavant, l'utilisateur est notifié par une alerte si l'image qu'il veut envoyer sur le serveur n'est pas supportée.

## Barre de chargement

Une barre de chargement est affichée à l'utilisateur durant l'application des algorithmes. Ainsi, il ne se retrouve pas avec une page statique.

## Tests côté client

Des tests end-to-end ont été ajoutés pour tester certaines fonctionnalités du client. Les tests sont réalisés à l'aide du cadriciel Nightwatch.
Ces tests se focalisent sur le bon fonctionnement des `inputs`, et vérifient que les éléments s'affichent correctement.

## Dockerfile
Nous avons ajouté un Dockerfile pour le backend ainsi que pour le frontend. Cela permet de déployer aisément l'ensemble.
En outre, l'ensemble peut être lancé via `docker-compose build&&docker-compose up` (depuis la racine du dépôt).
L'application est ensuite accessible à <http://localhost:8081/>.

# Fonctionnalités souhaitées mais irréalisables

## Supporter les images transparentes (PNG)

Nous avions prévu d'ajouter le support des images contenant de la transparence.
Cependant, après de longues recherches concernant la façon de connaître la valeur de transparence d'un pixel,
il se trouve que la bibliothèque ImgLib2 ne permet pas d'obtenir cette valeur.
Comme ImgLib2 ne supporte pas les PNG, nous ne pouvons pas les supporter. 

## Redimensionner une image

Nous voulions avoir la possibilité de redimensionner une image pour pouvoir envoyer au client seulement des miniatures des images.
Cela aurait été utile lorsque le client affiche la liste des images présentes sur le serveur.
Pouvoir avoir des images réduites permettrait un temps de chargement bien moindre et une utilisation de données plus faible.  
Le problème vient encore d'ImgLib2 qui ne permet pas vraiment de redimensionner une image. Il est possible d'avoir une vue sur seulement certains pixels (ce qui revient à réduire l'image).
Cependant, lorsque nous avons voulu tester sur une image en couleur, celle-ci s'est retrouvée convertie en nuance de gris, alors que le code n'est pas censé la convertir.  
Nous avons préféré ne pas ajouter cette fonctionnalité car elle aurait été incomplète et nous ne voulions pas de chose qui ne fonctionne qu'à moitié.

## Utiliser la Validation JSR 303
Nous voulions utiliser la Validation JSR 303 pour la validation des données. Face à du code dupliqué, nous avons opté pour une meilleure séparation des classes Java.
