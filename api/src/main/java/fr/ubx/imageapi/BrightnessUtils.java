package fr.ubx.imageapi;

import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utility class to perform brightness modification of an {@link net.imglib2.img.Img}.
 */
public class BrightnessUtils {
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * Change the brightness of the image.
	 * The input image is not modified.
	 * A new one is copied from the input image and returned with the new brightness.
	 * <p>
	 * If {@code brightnessDelta} is negative, the brightness of the images is reduced.
	 * If {@code brightnessDelta} is positive, the brightness of the image is augmented.
	 *
	 * @param image           the image on which the brightness is modified.
	 * @param brightnessDelta the delta value to add with the brightness of the image.
	 * @return a copied image of the input with its brightness modified.
	 */
	public static Img<UnsignedByteType> changeBrightness(Img<UnsignedByteType> image, int brightnessDelta) {
		LOGGER.debug("BrightnessUtils#changeBrightness() : Start");
		Img<UnsignedByteType> output = image.copy();
		final RandomAccess<UnsignedByteType> randAccessIn = image.randomAccess();
		final RandomAccess<UnsignedByteType> randAccessOut = output.randomAccess();

		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);
		final boolean HAS_COLOR = image.numDimensions() == 3;

		LOGGER.debug("BrightnessUtils#changeBrightness() : Loop on each pixel of the image");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				randAccessIn.setPosition(x, 0);
				randAccessIn.setPosition(y, 1);
				randAccessOut.setPosition(x, 0);
				randAccessOut.setPosition(y, 1);
				if (HAS_COLOR) {
					//the image is in color, we change the brightness in all color channel (red, blue, green), between 0 and 255
					for (int channel = 0; channel < 3; channel++) {
						randAccessIn.setPosition(channel, 2);
						randAccessOut.setPosition(channel, 2);
						randAccessOut.get().set(Math.max(Math.min(randAccessIn.get().get() + brightnessDelta, 255), 0));
					}
				} else {
					//the image is in grey scale, we change the brightness, between 0 and 255
					randAccessOut.get().set(Math.max(Math.min(randAccessIn.get().get() + brightnessDelta, 255), 0));
				}
			}
		}
		LOGGER.debug("BrightnessUtils#changeBrightness() : End");
		return output;
	}

	/**
	 * This method equalize the histogram of the image and return a copy of the image with its histogram equalized, without modification of the input image.
	 * <p>
	 * The image need to be in grey scale.
	 *
	 * @param image the image for which to perform the equalization.
	 * @return a copied image of the input with its histogram equalized.
	 * @throws UnsupportedOperationException if the image have a color dimension (i.e. the image is in color).
	 */
	public static Img<UnsignedByteType> equalizeHistogramGreyScale(Img<UnsignedByteType> image) {
		LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : Start");
		if (image.numDimensions() > 2) {
			LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : Error");
			throw new UnsupportedOperationException("The image should not have a color dimension");
		}
		int[] histogram = new int[256], cumulativeHistogram = new int[256];

		LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : Create histogram of the image");
		image.cursor().forEachRemaining(pixel -> histogram[pixel.get()]++);
		LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : Create cumulative histogram of the image");
		cumulativeHistogram[0] = histogram[0];
		for (int i = 1; i < histogram.length; i++) {
			cumulativeHistogram[i] = cumulativeHistogram[i - 1] + histogram[i];
		}

		final int N = (int) (image.max(0) * image.max(1));

		Img<UnsignedByteType> output = image.copy();
		final RandomAccess<UnsignedByteType> randAccessIn = image.randomAccess();
		final RandomAccess<UnsignedByteType> randAccessOut = output.randomAccess();

		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);

		LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : Loop on each pixel of the image and change each value");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				randAccessIn.setPosition(x, 0);
				randAccessIn.setPosition(y, 1);
				randAccessOut.setPosition(x, 0);
				randAccessOut.setPosition(y, 1);
				randAccessOut.get().set(cumulativeHistogram[randAccessOut.get().get()] * 255 / N);
			}
		}
		LOGGER.debug("BrightnessUtils#equalizeHistogramGreyScale() : End");
		return output;
	}

	/**
	 * This method equalize the histogram of the image and return a copy of the image with its histogram equalized, without modification of the input image.
	 * <p>
	 * The image need to be in color.
	 * The equalization of the histogram is done in the channel given in argument.
	 *
	 * @param image   the image for which to perform the equalization.
	 * @param channel the channel to use in the HSB model. It need be either 1 (Saturation) or 2 (Brightness).
	 * @return a copied image of the input with its histogram equalized.
	 * @throws UnsupportedOperationException if the argument {@code channel} is not 1 or 2 or if the image does not have a color dimension.
	 */
	public static Img<UnsignedByteType> equalizeHistogramColor(Img<UnsignedByteType> image, int channel) throws UnsupportedOperationException {
		LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Start");
		if (image.numDimensions() < 2) {
			LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Error");
			throw new UnsupportedOperationException("The image does not have a color dimension");
		}
		if (channel < 1 || channel > 2) {
			LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Error");
			throw new UnsupportedOperationException("The channel need to be either 1 or 2. It can't be : " + channel);
		}
		Img<UnsignedByteType> output = image.copy();
		final RandomAccess<UnsignedByteType> randAccessOut = output.randomAccess();
		final int WIDTH = (int) image.max(0), HEIGHT = (int) image.max(1);

		//PRECISION is the number of value to take in the float range [0,1] for the brightness
		//100 means we choose an int range [0,100]. The value of brightness is a float with 2 digits after the comma.
		//You can interpret it as "How many digits are left after it has been truncated?"
		final int PRECISION = 100;

		LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Create histogram of the image");
		int[] hist = new int[PRECISION + 1];//plus one because range [0-1] and not [0-1[
		for (int x = 0; x <= WIDTH; x++) {
			for (int y = 0; y <= HEIGHT; y++) {
				float[] rgb = new float[3];
				randAccessOut.setPosition(x, 0);
				randAccessOut.setPosition(y, 1);

				for (int color = 0; color < rgb.length; color++) {
					randAccessOut.setPosition(color, 2);
					rgb[color] = randAccessOut.get().get() / 256F;//change range [0, 256[ to [0, 1[
				}
				float[] hsb = ColorUtils.RGBtoHSB(rgb[0], rgb[1], rgb[2]);
				int brightness = (int) (hsb[channel] * PRECISION);//we change the range [0,1] to [0, 100]
				hist[brightness]++;
			}
		}
		LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Create cumulative histogram of the image");
		int[] cfd = new int[PRECISION + 1];
		cfd[0] = hist[0];
		for (int i = 1; i < hist.length; i++) {
			cfd[i] = cfd[i - 1] + hist[i];
		}

		//<number_of_value_in_the_range> / <number_of_pixel_in_image> / <precision>
		final float N = (PRECISION - 1F) / (image.max(0) * image.max(1)) / PRECISION;

		LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : Loop on each pixel of the image and assign new values");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				randAccessOut.setPosition(x, 0);
				randAccessOut.setPosition(y, 1);
				//get the rgb value of the pixel
				float[] rgb = new float[3];
				for (int colorChannel = 0; colorChannel < rgb.length; colorChannel++) {
					randAccessOut.setPosition(colorChannel, 2);
					rgb[colorChannel] = randAccessOut.get().get() / 256F;
				}
				//get the brightness of the pixel
				float[] hsb = ColorUtils.RGBtoHSB(rgb[0], rgb[1], rgb[2]);
				int brightness = (int) (hsb[channel] * PRECISION);
				//get the new brightness of the pixel
				hsb[channel] = cfd[brightness] * N;
				rgb = ColorUtils.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
				//set the new rgb value of the pixel
				for (int colorChannel = 0; colorChannel < rgb.length; colorChannel++) {
					randAccessOut.setPosition(colorChannel, 2);
					randAccessOut.get().set((int) (rgb[colorChannel] * 256F));
				}
			}
		}
		LOGGER.debug("BrightnessUtils#equalizeHistogramColorHsbChannel() : End");
		return output;
	}

	/**
	 * This method equalize the histogram of the image and return a copy of the image with its histogram equalized, without modification of the input image.
	 * <p>
	 * We assume that the image is in color.
	 * Each color channel of the image is equalized as if it was in grey scale.
	 * You should use {@link BrightnessUtils#equalizeHistogramColor(Img, int)} instead of this one because the result is better.
	 *
	 * @param image the image for which to perform the equalization.
	 * @return a copied image of the input with its histogram equalized.
	 */
	private static Img<UnsignedByteType> equalizeHistogramColor(Img<UnsignedByteType> image) {
		LOGGER.debug("BrightnessUtils#equalizeHistogramColor() : Start");
		//we create the histogram for each color. histogram[0]:red ; histogram[1]:green ; histogram[2]:blue
		int[][] histogram = new int[3][256];
		final RandomAccess<UnsignedByteType> randAccessIn = image.randomAccess();
		final int WIDTH = (int) image.max(0), HEIGHT = (int) image.max(1);
		LOGGER.debug("BrightnessUtils#equalizeHistogramColor() : Create histogram of the image");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				randAccessIn.setPosition(x, 0);
				randAccessIn.setPosition(y, 1);
				for (int color = 0; color < 3; color++) {
					randAccessIn.setPosition(color, 2);
					histogram[color][randAccessIn.get().get()]++;
				}
			}
		}
		LOGGER.debug("BrightnessUtils#equalizeHistogramColor() : Create cumulative histogram of the image");
		int[][] cumulativeHistogram = new int[3][256];
		for (int color = 0; color < 3; color++) {
			cumulativeHistogram[color][0] = histogram[color][0];
			for (int i = 1; i < histogram[color].length; i++) {
				cumulativeHistogram[color][i] = cumulativeHistogram[color][i - 1] + histogram[color][i];
			}
		}

		Img<UnsignedByteType> output = image.copy();
		final RandomAccess<UnsignedByteType> randAccessOut = output.randomAccess();
		float N = 255F / (output.max(0) * output.max(1));

		LOGGER.debug("BrightnessUtils#equalizeHistogramColor() : Loop on each pixel of the image and assign new values");
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				randAccessOut.setPosition(x, 0);
				randAccessOut.setPosition(y, 1);
				for (int color = 0; color < 3; color++) {
					randAccessOut.setPosition(color, 2);
					int value = (int) (cumulativeHistogram[color][randAccessOut.get().get()] * N);
					randAccessOut.get().set(value);
				}
			}
		}
		return output;
	}
}
