package fr.ubx.imageapi;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.Stream;

/**
 * Utility class to perform filter manipulation on an {@link net.imglib2.img.Img}.
 */
public class FilterUtils {
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * Change the hue of the image, this image need to be in color.
	 * The input image is not modified.
	 * A new one is copied from the input image and returned with its hue modified.
	 * <p>
	 * The hue should be in the range [0, 360[.
	 *
	 * @param image The image on which to apply the color filter.
	 * @param hue   The new hue of the image.
	 * @return A copy of the image with its hue modified.
	 * @throws UnsupportedOperationException If the image does not have a color dimension.
	 */
	public static Img<UnsignedByteType> hueFilter(Img<UnsignedByteType> image, float hue) throws UnsupportedOperationException {
		LOGGER.debug("FilterUtils#hueFilter() : Start");
		if (image.numDimensions() < 3) {
			LOGGER.debug("FilterUtils#hueFilter() : Error");
			throw new UnsupportedOperationException("The image does not have a color dimension");
		}

		Img<UnsignedByteType> output = image.copy();
		RandomAccess<UnsignedByteType> outputAccess = output.randomAccess();

		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);
		LOGGER.debug("FilterUtils#hueFilter() : Loop on each pixel and apply the new hue (" + hue + ")");
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				outputAccess.setPosition(x, 0);
				outputAccess.setPosition(y, 1);
				float[] rgb = new float[3];
				for (int color = 0; color < 3; color++) {
					outputAccess.setPosition(color, 2);
					rgb[color] = ((float) outputAccess.get().get()) / 255.0F;
				}
				float[] hsb = ColorUtils.RGBtoHSB(rgb[0], rgb[1], rgb[2]);
				rgb = ColorUtils.HSBtoRGB(hue, hsb[1], hsb[2]);
				for (int color = 0; color < 3; color++) {
					outputAccess.setPosition(color, 2);
					outputAccess.get().set((int) (rgb[color] * 255.0F));
				}
			}
		}
		LOGGER.debug("FilterUtils#hueFilter() : End");
		return output;
	}

	/**
	 * Create a new copy of the image and apply a mean blur filter with a blur level given in the parameter.
	 * The input image is not modified.
	 * <p>
	 * The blur level should be a odd positive integer. It is the size of the kernel used for the convolution.
	 * <p>
	 * For example : a kernel of 3x3 have a blur level of 3. a kernel of 5x5 have a blur level of 5.
	 *
	 * @param image     the image on which to apply the mean filter.
	 * @param blurLevel the blur level of the mean filter.
	 * @return A copy of the image with the mean filter applied.
	 */
	public static Img<UnsignedByteType> meanBlurFilter(Img<UnsignedByteType> image, int blurLevel) {
		LOGGER.debug("FilterUtils#meanBlurFilter() : Start");
		Img<UnsignedByteType> output = image.copy();
		RandomAccess<UnsignedByteType> outputAccess = output.randomAccess();
		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);

		final boolean HAS_COLOR = image.numDimensions() > 2;
		LOGGER.debug("FilterUtils#meanBlurFilter() : The image is in " + (HAS_COLOR ? "color" : "grey scale"));

		int radius = blurLevel / 2;
		LOGGER.debug("FilterUtils#meanBlurFilter() : The radius of the kernel is " + radius);

		//expand the image to be able to blur pixels on the edges.
		//expand of <blurLevel> pixels in first dimension (x) and <blurLevel> pixels in second dimension (y)
		IntervalView<UnsignedByteType> intervalView = HAS_COLOR ?
				Views.expandZero(image, blurLevel, blurLevel, 0) :// if image is in color, expand of O pixels in third dimension (color)
				Views.expandZero(image, blurLevel, blurLevel);
		RandomAccess<UnsignedByteType> intervalAccess = intervalView.randomAccess();

		LOGGER.debug("FilterUtils#meanBlurFilter() : Loop on each pixel of the image");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				if (HAS_COLOR) {
					for (int color = 0; color < 3; color++) {
						int value = 0;
						//loop on interval for mean filter
						for (int i = x - radius; i < x + radius; i++) {
							for (int j = y - radius; j < y + radius; j++) {
								intervalAccess.setPosition(i, 0);
								intervalAccess.setPosition(j, 1);
								intervalAccess.setPosition(color, 2);
								value += intervalAccess.get().get();
							}
						}
						outputAccess.setPosition(x, 0);
						outputAccess.setPosition(y, 1);
						outputAccess.setPosition(color, 2);
						outputAccess.get().set(value / (blurLevel * blurLevel));
					}
				} else {
					int value = 0;
					//loop on interval for mean filter
					for (int i = x - radius; i <= x + radius; i++) {
						for (int j = y - radius; j <= y + radius; j++) {
							intervalAccess.setPosition(i, 0);
							intervalAccess.setPosition(j, 1);
							value += intervalAccess.get().get();
						}
					}
					outputAccess.setPosition(x, 0);
					outputAccess.setPosition(y, 1);
					outputAccess.get().set(value / (blurLevel * blurLevel));
				}
			}
		}
		LOGGER.debug("FilterUtils#meanBlurFilter() : End");
		return output;
	}

	/**
	 * Apply a Gauss blur filter on a copy of the image. This copy of the image is then returned.
	 * The input image is not modified.
	 * <p>
	 * Bigger is {@code sigma}, bigger is the blur.
	 *
	 * @param image the image on which to apply the gauss filter.
	 * @param sigma the standard deviation of the isotropic Gaussian.
	 * @return A copy of the image with the Gauss filter applied.
	 */
	public static Img<UnsignedByteType> gaussBlurFilter(Img<UnsignedByteType> image, double sigma) {
		LOGGER.debug("FilterUtils#gaussBlurFilter() : Start");
		LOGGER.debug("FilterUtils#gaussBlurFilter() : The value of sigma is : " + sigma);
		Img<UnsignedByteType> output = image.copy();
		RandomAccessible<UnsignedByteType> infiniteImg = Views.extendZero(image);
		LOGGER.debug("FilterUtils#gaussBlurFilter() : Apply Gauss filter");
		Gauss3.gauss(sigma, infiniteImg, output);
		LOGGER.debug("FilterUtils#gaussBlurFilter() : End");
		return output;
	}

	/**
	 * Apply a Sobel filter on a copy of the image. This filter is used to detect edges in the images.
	 * The input image is not modified.
	 *
	 * @param image the image on which to apply the Sobel filter.
	 * @return A copy of the image with the Sobel filter applied.
	 */
	public static Img<UnsignedByteType> outlineFilter(Img<UnsignedByteType> image) {
		LOGGER.debug("FilterUtils#outlineFilter(Img) : Start");
		final boolean HAS_COLOR = image.numDimensions() > 2;
		LOGGER.debug("FilterUtils#outlineFilter(Img) : The image is in " + (HAS_COLOR ? "color" : "grey scale"));
		Img<UnsignedByteType> output = HAS_COLOR ? colorImgToGreyImg(image) : image.copy();

		RandomAccess<UnsignedByteType> outputAccess = output.randomAccess();
		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);
		final int[][] HOR_KERNEL = new int[][]{{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
		final int[][] VER_KERNEL = new int[][]{{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

		//expand the image to be able to blur pixels on the edges.
		//expand of 1 pixels in first dimension (x) and 1 pixels in second dimension (y)
		IntervalView<UnsignedByteType> intervalView = HAS_COLOR ? Views.expandZero(image, 1, 1, 0) : Views.expandZero(image, 1, 1);
		RandomAccess<UnsignedByteType> intervalAccess = intervalView.randomAccess();

		LOGGER.debug("FilterUtils#outlineFilter(Img) : Loop on each pixel of the image and create gradients for the outline");
		for (int y = 0; y <= HEIGHT; ++y) {
			for (int x = 0; x <= WIDTH; ++x) {
				int horizontalGradient = 0;
				int verticalGradient = 0;
				int l = 0;
				//loop on interval for outline filter
				for (int j = y - 1; j <= y + 1; j++) {
					int k = 0;
					for (int i = x - 1; i <= x + 1; i++) {
						intervalAccess.setPosition(i, 0);
						intervalAccess.setPosition(j, 1);
						if (HAS_COLOR) {
							intervalAccess.setPosition(0, 2);
						}
						horizontalGradient += intervalAccess.get().get() * HOR_KERNEL[l][k];
						verticalGradient += intervalAccess.get().get() * VER_KERNEL[l][k];
						k++;
					}
					l++;
				}
				outputAccess.setPosition(x, 0);
				outputAccess.setPosition(y, 1);
				//the value set is the magnitude of the two gradients
				int value = (int) Math.sqrt(Math.pow(horizontalGradient, 2) + Math.pow(verticalGradient, 2));
				if (HAS_COLOR) {
					//set the value on each channel to keep the image in grey scale
					for (int color = 0; color < 3; color++) {
						outputAccess.setPosition(color, 2);
						outputAccess.get().set(value);
					}
				} else {
					outputAccess.get().set(value);
				}
			}
		}
		LOGGER.debug("FilterUtils#outlineFilter(Img) : End");
		return output;
	}

	/**
	 * Apply a Sobel filter on a copy of the image then threshold it to have clearer outline. This filter is used to detect edges in the images.
	 * The input image is not modified.
	 * This method calls {@link FilterUtils#outlineFilter(Img)} to apply the Sobel filter.
	 *
	 * @param image     the image on which to apply the Sobel filter.
	 * @param threshold the threshold for the outline. It should be in the range [0, 255].
	 * @return A copy of the image with the Sobel filter applied.
	 */
	public static Img<UnsignedByteType> outlineFilter(Img<UnsignedByteType> image, int threshold) {
		LOGGER.debug("FilterUtils#outlineFilter(Img, int) : Start");
		Img<UnsignedByteType> output = outlineFilter(image);
		RandomAccess<UnsignedByteType> outputAccess = output.randomAccess();
		final int WIDTH = (int) image.max(0);
		final int HEIGHT = (int) image.max(1);
		LOGGER.debug("FilterUtils#outlineFilter(Img, int) : Apply threshold");
		for (int y = 0; y <= HEIGHT; y++) {
			for (int x = 0; x <= WIDTH; x++) {
				outputAccess.setPosition(x, 0);
				outputAccess.setPosition(y, 1);
				if (image.numDimensions() > 2) {
					for (int color = 0; color < 3; color++) {
						outputAccess.setPosition(color, 2);
						outputAccess.get().set(outputAccess.get().get() < threshold ? 0 : 255);
					}
				} else {
					outputAccess.get().set(outputAccess.get().get() < threshold ? 0 : 255);

				}
			}
		}
		LOGGER.debug("FilterUtils#outlineFilter(Img, int) : End");
		return output;
	}

	/**
	 * Convert a colored image in grey scale.
	 * The input image is not modified. Copy is made, converted and then returned.
	 * <p>
	 * It is assumed that {@code colorImage} is effectively in color.
	 *
	 * @param colorImage the image to convert in grey scale.
	 * @return the copy of the image in grey scale.
	 */
	private static Img<UnsignedByteType> colorImgToGreyImg(Img<UnsignedByteType> colorImage) {
		LOGGER.debug("FilterUtils#colorImgToGreyImg() : Start");
		Img<UnsignedByteType> output = colorImage.copy();
		final RandomAccess<UnsignedByteType> outputAccess = output.randomAccess();

		final int WIDTH = (int) colorImage.max(0);
		final int HEIGHT = (int) colorImage.max(1);

		LOGGER.debug("FilterUtils#colorImgToGreyImg() : Loop on each pixel and convert to grey scale");
		for (int x = 0; x <= WIDTH; ++x) {
			for (int y = 0; y <= HEIGHT; ++y) {
				outputAccess.setPosition(x, 0);
				outputAccess.setPosition(y, 1);

				int[] rgb = new int[]{0, 0, 0};
				Stream.of(0, 1, 2).forEach(v -> {
					outputAccess.setPosition(v, 2);
					rgb[v] = outputAccess.get().get();
				});
				for (int w = 0; w < 3; w++) {
					outputAccess.setPosition(w, 2);
					outputAccess.get().set((int) (0.3 * rgb[0] + 0.59 * rgb[1] + 0.11 * rgb[2]));
				}
			}
		}
		LOGGER.debug("FilterUtils#colorImgToGreyImg() : End");
		return output;
	}
}
