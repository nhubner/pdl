package fr.ubx.imageapi;

import java.util.stream.Stream;

/**
 * Package-private class to perform color model conversion.
 * It is not intended to be used outside of the package.
 */
class ColorUtils {

	/**
	 * Converts the components of a color in the RGB model, to an equivalent set of values for hue, saturation,
	 * and brightness that are the three components of the HSB model.
	 * <p>
	 * Red, Green, Blue should be in the range [0, 1].
	 * Hue is in range [0, 360[. Saturation and brightness in range [0, 1].
	 *
	 * @param red   the red component of the color.
	 * @param green the green component of the color.
	 * @param blue  the blue component of the color.
	 * @return an array of three elements containing the hue, saturation, and brightness (in that order).
	 */
	static float[] RGBtoHSB(float red, float green, float blue) {
		float max = Stream.of(red, green, blue).max(Float::compareTo).get();
		float min = Stream.of(red, green, blue).min(Float::compareTo).get();
		float chroma = max - min;

		float hue;
		if (chroma == 0.0F) {
			hue = 0.0F;
		} else if (max == red) {
			hue = 60.0F * ((green - blue) / chroma);
		} else if (max == green) {
			hue = 60.0F * (2.0F + (blue - red) / chroma);
		} else if (max == blue) {
			hue = 60.0F * (4.0F + (red - green) / chroma);
		} else {
			hue = 0.0F;
		}

		float saturation;
		if (max == 0.0F) {
			saturation = 0.0F;
		} else {
			saturation = chroma / max;
		}

		return new float[]{hue, saturation, max};
	}

	/**
	 * Converts the components of a color in the HSB model, to an equivalent set of values for red, green, blue
	 * that are the three components of the RGB model.
	 * <p>
	 * Hue should be in range [0, 360[. Saturation and brightness should be in range [0, 1].
	 * Red, Green, Blue are in the range [0, 1].
	 *
	 * @param hue        the hue component of the color.
	 * @param saturation the saturation component of the color.
	 * @param brightness the brightness component of the color.
	 * @return an array of three elements containing the red, green, and blue (in that order).
	 */
	static float[] HSBtoRGB(float hue, float saturation, float brightness) {
		float chroma = brightness * saturation;
		float h = hue / 60F;
		float x = chroma * (1.0F - Math.abs((h % 2.0F) - 1.0F));
		float[] rgb;
		if (0.0F <= h && h <= 1.0F) {
			rgb = new float[]{chroma, x, 0.0F};
		} else if (1.0F < h && h <= 2.0F) {
			rgb = new float[]{x, chroma, 0.0F};
		} else if (2.0F < h && h <= 3.0F) {
			rgb = new float[]{0.0F, chroma, x};
		} else if (3.0F < h && h <= 4.0F) {
			rgb = new float[]{0.0F, x, chroma};
		} else if (4.0F < h && h <= 5.0F) {
			rgb = new float[]{x, 0.0F, chroma};
		} else if (5.0F < h && h <= 6.0F) {
			rgb = new float[]{chroma, 0.0F, x};
		} else {
			rgb = new float[]{0.0F, 0.0F, 0.0F};
		}
		float m = brightness - chroma;
		for (int i = 0; i < rgb.length; i++) {
			rgb[i] += m;
		}
		return rgb;
	}
}
