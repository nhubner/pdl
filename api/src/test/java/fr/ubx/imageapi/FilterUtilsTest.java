package fr.ubx.imageapi;

import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class FilterUtilsTest {
	public static Img<UnsignedByteType> greyImage;
	public static Img<UnsignedByteType> colorImage;

	@BeforeAll
	public static void initialisation() {
		ClassLoader classLoader = BrightnessUtilsTest.class.getClassLoader();

		String greyImagePath = classLoader.getResource("fr/ubx/imageapi/files/image-1.tif").getPath();
		String colorImagePath = classLoader.getResource("fr/ubx/imageapi/files/paysage.jpg").getPath();

		final ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
		final ImgOpener imgOpener = new ImgOpener();
		greyImage = imgOpener.openImgs(greyImagePath, factory).get(0);
		colorImage = imgOpener.openImgs(colorImagePath, factory).get(0);
		imgOpener.context().dispose();
	}

	@Test
	public void colorFilterTest() {
		assertThrows(UnsupportedOperationException.class, () -> FilterUtils.hueFilter(greyImage, 50));
		assertDoesNotThrow(() -> FilterUtils.hueFilter(colorImage, 134));
		//It is not possible to test if pixels are set as it should because casting float to int change a little bit the value.
		//Values are different enough to fail the test, but are not enough different enough to significantly change the image result visually.
		//For example, if we set a hue of 50 on each pixel, they will be in the range [49, 51] and rarely outside of this range. And if the pixels was grey, the hue is set to 0 (and that made tests impossible).
	}

	@Test
	public void meanBlurFilterTest() {
		assertDoesNotThrow(() -> FilterUtils.meanBlurFilter(greyImage, 3));
		assertDoesNotThrow(() -> FilterUtils.meanBlurFilter(colorImage, 5));
		//Again, because of int casting in division, it is not possible to verify the value algorithmically.
		//But, visually, the blur is working as intended.
	}

	@Test
	public void gaussBlurFilterTest() {
		assertDoesNotThrow(() -> FilterUtils.gaussBlurFilter(greyImage, 3F/4F));
		assertDoesNotThrow(() -> FilterUtils.gaussBlurFilter(colorImage, 3F/5F));
		//Again, it is not possible to verify algorithmically is the gauss filter is applied correctly.
		//But, visually, the blur is working as indented
	}

	@Test
	public void outlineFilterTest() {
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(greyImage));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(greyImage, 70));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(greyImage, -5));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(greyImage, 267));

		assertDoesNotThrow(() -> FilterUtils.outlineFilter(colorImage));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(colorImage, 127));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(colorImage, -10));
		assertDoesNotThrow(() -> FilterUtils.outlineFilter(colorImage, 286));
	}
}
