package fr.ubx.imageapi;

import io.scif.img.ImgOpener;
import io.scif.img.ImgSaver;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class BrightnessUtilsTest {

	public static Img<UnsignedByteType> greyImage;
	public static Img<UnsignedByteType> colorImage;

	@BeforeAll
	public static void initialisation() {
		ClassLoader classLoader = BrightnessUtilsTest.class.getClassLoader();

		String greyImagePath = classLoader.getResource("fr/ubx/imageapi/files/image-1.tif").getPath();
		String colorImagePath = classLoader.getResource("fr/ubx/imageapi/files/paysage.jpg").getPath();

		final ArrayImgFactory<UnsignedByteType> factory = new ArrayImgFactory<>(new UnsignedByteType());
		final ImgOpener imgOpener = new ImgOpener();
		greyImage = imgOpener.openImgs(greyImagePath, factory).get(0);
		colorImage = imgOpener.openImgs(colorImagePath, factory).get(0);
		imgOpener.context().dispose();
	}

	@Test
	public void changeBrightnessTest() {
		final int BRIGHTNESS_DELTA = 10;
		//test method do not throw exceptions
		assertDoesNotThrow(() -> BrightnessUtils.changeBrightness(greyImage, BRIGHTNESS_DELTA));
		assertDoesNotThrow(() -> BrightnessUtils.changeBrightness(colorImage, BRIGHTNESS_DELTA));

		//test method do not modify input and return a new object
		Img<UnsignedByteType> beforeGrey = greyImage;
		Img<UnsignedByteType> returnValueGrey = BrightnessUtils.changeBrightness(greyImage, BRIGHTNESS_DELTA);
		assertNotEquals(greyImage, returnValueGrey);
		assertEquals(greyImage, beforeGrey);

		//test brightness change on grey-scale image is well done
		Random rand = new Random();
		RandomAccess<UnsignedByteType> randomAccessReturned = returnValueGrey.randomAccess();
		RandomAccess<UnsignedByteType> randomAccess = greyImage.randomAccess();
		for (int i = 0; i < 20; i++) {
			int x = rand.nextInt((int) returnValueGrey.max(0));
			randomAccessReturned.setPosition(x, 0);
			randomAccessReturned.setPosition(i, 1);
			randomAccess.setPosition(x, 0);
			randomAccess.setPosition(i, 1);
			assertEquals(Math.min(255, randomAccess.get().get() + BRIGHTNESS_DELTA), randomAccessReturned.get().get(), "failure on x=" + x + ", y=" + i);
		}

		//test method do not modify input and return a new object
		Img<UnsignedByteType> beforeColor = colorImage;
		Img<UnsignedByteType> returnValueColor = BrightnessUtils.changeBrightness(colorImage, BRIGHTNESS_DELTA);
		assertNotEquals(colorImage, returnValueColor);
		assertEquals(colorImage, beforeColor);

		//test brightness change on color image is well done
		Random randColor = new Random();
		RandomAccess<UnsignedByteType> randomAccessReturnedColor = returnValueColor.randomAccess();
		RandomAccess<UnsignedByteType> randomAccessColor = colorImage.randomAccess();
		for (int color = 0; color < 3; color++) {
			randomAccessReturnedColor.setPosition(color, 2);
			randomAccessColor.setPosition(color, 2);
			for (int i = 0; i < 20; i++) {
				int x = randColor.nextInt((int) returnValueGrey.max(0));
				randomAccessReturnedColor.setPosition(x, 0);
				randomAccessReturnedColor.setPosition(i, 1);
				randomAccessColor.setPosition(x, 0);
				randomAccessColor.setPosition(i, 1);
				assertEquals(Math.min(255, randomAccessColor.get().get() + BRIGHTNESS_DELTA), randomAccessReturnedColor.get().get(), "failure on x=" + x + ", y=" + i + ", z=" + color);
			}
		}
	}

	@Test
	public void equalizeHistogramColorTest() {
		//test method do not trow exceptions
		assertDoesNotThrow(() -> BrightnessUtils.equalizeHistogramGreyScale(greyImage));
		assertDoesNotThrow(() -> BrightnessUtils.equalizeHistogramColor(colorImage, 2));
		assertDoesNotThrow(() -> BrightnessUtils.equalizeHistogramColor(colorImage, 1));
		//test method do throw exceptions
		assertThrows(UnsupportedOperationException.class, () -> BrightnessUtils.equalizeHistogramColor(colorImage, 0));
		assertThrows(UnsupportedOperationException.class, () -> BrightnessUtils.equalizeHistogramColor(colorImage, 5));
	}
}
