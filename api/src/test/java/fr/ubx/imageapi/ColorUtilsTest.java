package fr.ubx.imageapi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ColorUtilsTest {

	@Test
	public void RGBtoHSBTest() {
		//We assume ColorUtils.HSBtoRGB() works fine
		//test random color
		final float RED = 30.0F / 255F;
		final float GREEN = 50.0F / 255F;
		final float BLUE = 70.0F / 255F;
		float[] hsb = ColorUtils.RGBtoHSB(RED, GREEN, BLUE);
		float[] rgb = ColorUtils.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
		assertEquals(RED, rgb[0], 0.1F);
		assertEquals(GREEN, rgb[1], 0.1F);
		assertEquals(BLUE, rgb[2], 0.1F);
		//test grey color
		final float RED1 = 30.0F / 255F;
		final float GREEN1 = 30.0F / 255F;
		final float BLUE1 = 30.0F / 255F;
		float[] hsb1 = ColorUtils.RGBtoHSB(RED1, GREEN1, BLUE1);
		float[] rgb1 = ColorUtils.HSBtoRGB(hsb1[0], hsb1[1], hsb1[2]);
		assertEquals(RED1, rgb1[0], 0.1F);
		assertEquals(GREEN1, rgb1[1], 0.1F);
		assertEquals(BLUE1, rgb1[2], 0.1F);
		//test black color
		final float RED2 = 0.0F / 255F;
		final float GREEN2 = 0.0F / 255F;
		final float BLUE2 = 0.0F / 255F;
		float[] hsb2 = ColorUtils.RGBtoHSB(RED2, GREEN2, BLUE2);
		float[] rgb2 = ColorUtils.HSBtoRGB(hsb2[0], hsb2[1], hsb2[2]);
		assertEquals(RED2, rgb2[0], 0.1F);
		assertEquals(GREEN2, rgb2[1], 0.1F);
		assertEquals(BLUE2, rgb2[2], 0.1F);
		//test white color
		final float RED3 = 1.0F;
		final float GREEN3 = 1.0F;
		final float BLUE3 = 1.0F;
		float[] hsb3 = ColorUtils.RGBtoHSB(RED3, GREEN3, BLUE3);
		float[] rgb3 = ColorUtils.HSBtoRGB(hsb3[0], hsb3[1], hsb3[2]);
		assertEquals(RED3, rgb3[0], 0.1F);
		assertEquals(GREEN3, rgb3[1], 0.1F);
		assertEquals(BLUE3, rgb3[2], 0.1F);
	}

	@Test
	public void HSBtoRGBTest() {
		//We assume ColorUtils.RGBtoHSB() works fine
		//test random color
		final float HUE = 10.0F;
		final float SATURATION = 0.2F;
		final float BRIGHTNESS = 0.8F;
		float[] rgb = ColorUtils.HSBtoRGB(HUE, SATURATION, BRIGHTNESS);
		float[] hsb = ColorUtils.RGBtoHSB(rgb[0], rgb[1], rgb[2]);
		assertEquals(HUE, hsb[0], 0.1F);
		assertEquals(SATURATION, hsb[1], 0.1F);
		assertEquals(BRIGHTNESS, hsb[2], 0.1F);
		//test grey color
		final float HUE1 = 0.0F;
		final float SATURATION1 = 0.0F;
		final float BRIGHTNESS1 = 0.6F;
		float[] rgb1 = ColorUtils.HSBtoRGB(HUE1, SATURATION1, BRIGHTNESS1);
		float[] hsb1 = ColorUtils.RGBtoHSB(rgb1[0], rgb1[1], rgb1[2]);
		assertEquals(HUE1, hsb1[0], 0.1F);
		assertEquals(SATURATION1, hsb1[1], 0.1F);
		assertEquals(BRIGHTNESS1, hsb1[2], 0.1F);
		//test black color
		final float HUE2 = 0.0F;
		final float SATURATION2 = 0.0F;
		final float BRIGHTNESS2 = 0.0F;
		float[] rgb2 = ColorUtils.HSBtoRGB(HUE2, SATURATION2, BRIGHTNESS2);
		float[] hsb2 = ColorUtils.RGBtoHSB(rgb2[0], rgb2[1], rgb2[2]);
		assertEquals(HUE2, hsb2[0], 0.1F);
		assertEquals(SATURATION2, hsb2[1], 0.1F);
		assertEquals(BRIGHTNESS2, hsb2[2], 0.1F);
		//test white color
		final float HUE3 = 0.0F;
		final float SATURATION3 = 0.0F;
		final float BRIGHTNESS3 = 1.0F;
		float[] rgb3 = ColorUtils.HSBtoRGB(HUE3, SATURATION3, BRIGHTNESS3);
		float[] hsb3 = ColorUtils.RGBtoHSB(rgb3[0], rgb3[1], rgb3[2]);
		assertEquals(HUE3, hsb3[0], 0.1F);
		assertEquals(SATURATION3, hsb3[1], 0.1F);
		assertEquals(BRIGHTNESS3, hsb3[2], 0.1F);
	}
}
